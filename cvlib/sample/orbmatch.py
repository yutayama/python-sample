# coding: utf-8

import cv2
from matplotlib import pyplot as plt
import numpy as np

# for type hints
from typing import Sequence, Tuple, Optional
from cv2 import KeyPoint, DMatch
from cv2.typing import MatLike


def adjust_ang(ang_min, ang_max):
    """Parameters
    input
    ang_min: start angle of degree
    ang_max: end angle of degree
    output
    unique_ang_min: angle after conversion to unique `ang_min`
    unique_ang_max: angle after conversion to unique `ang_max`
    """
    unique_ang_min = ang_min
    unique_ang_max = ang_max
    unique_ang_min %= 360
    unique_ang_max %= 360
    if unique_ang_min >= unique_ang_max:
        unique_ang_max += 360
    return unique_ang_min, unique_ang_max


def any_angle_only(mag, ang, ang_min, ang_max):
    """
    input
    mag: `cv2.cartToPolar` method `mag` reuslts
    ang: `cv2.cartToPolar` method `ang` reuslts
    ang_min: start angle of degree after `adjust_ang` function
    ang_max: end angle of degree after `adjust_ang` function
    output
    any_mag: array of replace any out of range `ang` with nan
    any_ang: array of replace any out of range `mag` with nan
    description
    Replace any out of range `mag` and `ang` with nan.
    """
    any_mag = np.copy(mag)
    any_ang = np.copy(ang)
    ang_min %= 360
    ang_max %= 360
    if ang_min < ang_max:
        any_mag[(ang < ang_min) | (ang_max < ang)] = np.nan
        any_ang[(ang < ang_min) | (ang_max < ang)] = np.nan
    else:
        any_mag[(ang_max < ang) & (ang < ang_min)] = np.nan
        any_ang[(ang_max < ang) & (ang < ang_min)] = np.nan
        any_ang[ang <= ang_max] += 360
    return any_mag, any_ang


def calc_opticalflow(
        keypoints1: Sequence[KeyPoint],
        keypoints2: Sequence[KeyPoint],
        matches: Sequence[DMatch],
        *args
        ) -> Tuple[MatLike]:
    pts1 = np.array([k.pt for k in keypoints1])
    pts2 = np.array([k.pt for k in keypoints2])
    
    src  = np.zeros((len(matches), 2))
    ref  = np.zeros((len(matches), 2))
    flow = np.zeros((len(matches), 2))

    for i, m in enumerate(matches):
        qid = m.queryIdx
        tid = m.trainIdx
        src[i] = pts1[qid]
        ref[i] = pts2[tid]
        flow[i] = pts2[tid] - pts1[qid]

    sx = src[..., 0]
    sy = src[..., 1]
    ex = ref[..., 0]
    ey = ref[..., 1]
    u = flow[..., 0]
    v = flow[..., 1]

    return src, ref, flow


def affine(img: MatLike) -> MatLike:

    if len(img.shape) == 2:
        h, w = img.shape
    else:
        h, w, _ = img.shape

    mat = cv2.getRotationMatrix2D(center=(w/2, h/2), angle=5, scale=1.0)
    affine_img = cv2.warpAffine(img, mat, (w, h))
    return affine_img


def flow2hsv(flow: MatLike) -> MatLike:

    shape = list(flow.shape)
    shape[-1] = 3
    hsv = np.zeros(shape, dtype='uint8')

    if len(shape) == 2:
        hsv = np.expand_dims(hsv, 0)
        flow = np.expand_dims(flow, 0)
    
    mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1], angleInDegrees=True)

    # ang_min, ang_max = 0, 360
    # _ang_min, _ang_max = adjust_ang(ang_min, ang_max)  # 角度の表現を統一する
    # any_mag, any_ang = any_angle_only(mag, ang, ang_min, ang_max)

    any_mag, any_ang = mag, ang
    _ang_min, _ang_max = 0, 360

    hsv[..., 0] = 180*(any_ang - _ang_min) / (_ang_max - _ang_min)
    hsv[..., 1] = 255
    hsv[..., 2] = cv2.normalize(any_mag, None, 0, 255, cv2.NORM_MINMAX)
    flow_rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

    flow_rgb = flow_rgb.reshape(shape)
    # print(flow_rgb)
    return flow_rgb


def drawMatchesArrow(
        image1: MatLike,
        keypoints1: Sequence[KeyPoint],
        image2: MatLike,
        keypoints2: Sequence[KeyPoint],
        matches1to2: Sequence[DMatch],
        outimg: Optional[MatLike],
        *args,
        hconcat=True,
        vconcat=False,
        **kwargs
        ) -> MatLike:
    
    if len(image1.shape) == 2:
        ih, iw = image1.shape
    else:
        ih, iw, _ = image1.shape

    src, ref, flow = calc_opticalflow(keypoints1, keypoints2, matches1to2)
    dst = ref.copy()
    flowcolors = flow2hsv(flow)

    if hconcat:
        # print("HCONCAT")
        outimg = np.concatenate([image1, image2], 1)
        dst[..., 0] += iw
    elif vconcat:
        # print("VCONCAT")
        outimg = np.concatenate([image1, image2], 0)
        dst[..., 1] += ih
    else:
        # print("VCONCAT")
        outimg = np.concatenate([image1], 0)

    if len(outimg.shape) == 2:
        oh, ow = outimg.shape
        outimg = cv2.cvtColor(outimg, cv2.COLOR_GRAY2RGB)
    else:
        oh, ow, _ = outimg.shape

    if hconcat or vconcat:
        for s, d in zip(src, dst):
            cv2.line(outimg, (int(s[0]), int(s[1])), (int(d[0]), int(d[1])), (0, 255, 0))

    for s, r, c in zip(src, ref, flowcolors):
        flowcolor = tuple([int(i) for i in c])
        cv2.arrowedLine(outimg, (int(s[0]), int(s[1])), (int(r[0]), int(r[1])), flowcolor)

    return outimg



def orbmatch(args):

    query_img = cv2.imread(args.src)
    train_img = cv2.imread(args.ref)
    train_img = affine(train_img)

    query_gray = cv2.cvtColor(query_img, cv2.COLOR_BGR2GRAY)
    train_gray = cv2.cvtColor(train_img, cv2.COLOR_BGR2GRAY)

    query_rgb = cv2.cvtColor(query_img, cv2.COLOR_BGR2RGB)
    train_rgb = cv2.cvtColor(train_img, cv2.COLOR_BGR2RGB)

    # Initiate ORB detector
    detector = cv2.ORB_create(nfeatures=1000)

    # find the keypoints and descriptors with ORB
    kp1, des1 = detector.detectAndCompute(query_gray, None)
    kp2, des2 = detector.detectAndCompute(train_gray, None)

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1, des2)

    # Sort them in the order of their distance.
    matches = filter(lambda x: x.distance < 20, matches)
    matches = sorted(matches, key=lambda x: x.distance)

    # Draw matches.
    # img3 = cv2.drawMatches(query_rgb, kp1, train_rgb, kp2, matches, None, flags=2)
    # Draw optical flows with HSV colored arrow
    img3 = drawMatchesArrow(query_gray, kp1, train_gray, kp2, matches, None, hconcat=False, vconcat=False, flags=2)

    if args.show:
        src, _, flow = calc_opticalflow(kp1, kp2, matches)
        x, y = src[..., 0], src[..., 1]
        u, v = flow[..., 0], flow[..., 1]
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 16))
        ax1.imshow(query_rgb)
        ax1.quiver(x, y, u, v, angles='xy', scale_units='xy', scale=1, color=[0.0, 1.0, 0.0])
        ax2.imshow(img3)
        plt.show()

    if args.show and False:
        x, y, u, v = calc_opticalflow(kp1, kp2, matches)
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 16))
        ax1.imshow(query_rgb)
        ax1.quiver(x, y, u, v, angles='xy', scale_units='xy', scale=1, color=[0.0, 1.0, 0.0])
        ax2.imshow(img3)
        plt.show()

    if False:
        plt.imshow(img3)
        plt.show()

    if args.out is not None:
        cv2.imwrite(args.out, img3)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(prog='main')
    parser.add_argument('src', type=str)
    parser.add_argument('ref', type=str)
    parser.add_argument('-o', '--out', default=None, type=str)
    parser.add_argument('-s', '--show', action='store_true')
    args = parser.parse_args()
    orbmatch(args)
