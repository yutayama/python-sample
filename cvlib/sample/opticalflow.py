# coding: utf-8

import cv2
from matplotlib import pyplot as plt
import numpy as np

# for type hints
from typing import Sequence
from cv2 import KeyPoint, DMatch
from cv2.typing import MatLike


def flow_vector(flow, spacing, margin, minlength):
    """Parameters:
    input
    flow: motion vectors 3D-array
    spacing: pixel spacing of the flow
    margin: pixel margins of the flow
    minlength: minimum pixels to leave as flow
    output
    x: x coord 1D-array
    y: y coord 1D-array
    u: x direction flow vector 2D-array
    v: y direction flow vector 2D-array
    """
    h, w, _ = flow.shape

    x = np.arange(margin, w - margin, spacing, dtype=np.int64)
    y = np.arange(margin, h - margin, spacing, dtype=np.int64)

    # np.ix_(y, x) で y, x で指定した位置の値を抽出
    mesh_flow = flow[np.ix_(y, x)]
    mag, _ = cv2.cartToPolar(mesh_flow[..., 0], mesh_flow[..., 1])
    mesh_flow[mag < minlength] = np.nan  # minlength以下をnanに置換

    u = mesh_flow[..., 0]
    v = mesh_flow[..., 1]

    return x, y, u, v


def adjust_ang(ang_min, ang_max):
    """Parameters
    input
    ang_min: start angle of degree
    ang_max: end angle of degree
    output
    unique_ang_min: angle after conversion to unique `ang_min`
    unique_ang_max: angle after conversion to unique `ang_max`
    """
    unique_ang_min = ang_min
    unique_ang_max = ang_max
    unique_ang_min %= 360
    unique_ang_max %= 360
    if unique_ang_min >= unique_ang_max:
        unique_ang_max += 360
    return unique_ang_min, unique_ang_max


def any_angle_only(mag, ang, ang_min, ang_max):
    """
    input
    mag: `cv2.cartToPolar` method `mag` reuslts
    ang: `cv2.cartToPolar` method `ang` reuslts
    ang_min: start angle of degree after `adjust_ang` function
    ang_max: end angle of degree after `adjust_ang` function
    output
    any_mag: array of replace any out of range `ang` with nan
    any_ang: array of replace any out of range `mag` with nan
    description
    Replace any out of range `mag` and `ang` with nan.
    """
    any_mag = np.copy(mag)
    any_ang = np.copy(ang)
    ang_min %= 360
    ang_max %= 360
    if ang_min < ang_max:
        any_mag[(ang < ang_min) | (ang_max < ang)] = np.nan
        any_ang[(ang < ang_min) | (ang_max < ang)] = np.nan
    else:
        any_mag[(ang_max < ang) & (ang < ang_min)] = np.nan
        any_ang[(ang_max < ang) & (ang < ang_min)] = np.nan
        any_ang[ang <= ang_max] += 360
    return any_mag, any_ang

def hsv_cmap(ang_min, ang_max, size):
    """
    input
    ang_min: start angle of degree after `adjust_ang` function
    ang_max: end angle of degree after `adjust_ang` function
    size: map px size
    output
    hsv_cmap_rgb: HSV color map in radial vector flow
    x, y, u, v: radial vector flow value
    x: x coord 1D-array
    y: y coord 1D-array
    u: x direction flow vector 2D-array
    v: y direction flow vector 2D-array
    description
    Create a normalized hsv colormap between `ang_min` and `ang_max`.
    """
    # 放射状に広がるベクトル場の生成
    half = size // 2
    x = np.arange(-half, half+1, 1, dtype=np.float64)
    y = np.arange(-half, half+1, 1, dtype=np.float64)
    u, v = np.meshgrid(x, y)

    # HSV色空間の配列に入れる
    hsv = np.zeros((len(y), len(x), 3), dtype='uint8')
    mag, ang = cv2.cartToPolar(u, v, angleInDegrees=True)
    any_mag, any_ang = any_angle_only(mag, ang, ang_min, ang_max)
    hsv[..., 0] = 180*(any_ang - ang_min) / (ang_max - ang_min)
    hsv[..., 1] = 255
    hsv[..., 2] = cv2.normalize(any_mag, None, 0, 255, cv2.NORM_MINMAX)
    hsv_cmap_rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

    return hsv_cmap_rgb, x, y, u, v


def affine(img: MatLike) -> MatLike:

    if len(img.shape) == 2:
        h, w = img.shape
    else:
        h, w, _ = img.shape

    mat = cv2.getRotationMatrix2D(center=(w/2, h/2), angle=5, scale=1.0)
    affine_img = cv2.warpAffine(img, mat, (w, h))
    return affine_img

def opticalflow(args):

    prev_frame = cv2.imread(args.src)
    next_frame = cv2.imread(args.ref)
    next_frame = affine(next_frame)

    prev_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
    next_gray = cv2.cvtColor(next_frame, cv2.COLOR_BGR2GRAY)

    prev_rgb = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2RGB)
    next_rgb = cv2.cvtColor(next_frame, cv2.COLOR_BGR2RGB)

    # display prev and next images
    if args.dispimg:
        fig, axs = plt.subplots(2, 2, figsize=(20, 16))
        axs[0, 0].set_title('prev_frame')
        axs[0, 0].imshow(prev_rgb)
        axs[0, 1].set_title('next_frame')
        axs[0, 1].imshow(next_rgb)
        axs[1, 0].set_title('prev_gray')
        axs[1, 0].imshow(prev_gray, cmap='gray')
        axs[1, 1].set_title('next_gray')
        axs[1, 1].imshow(next_gray, cmap='gray')

        plt.show()

    # calculating optical flow
    # flow は [h][w][2=(mvx, mvy)]のoptical flow
    flow = cv2.calcOpticalFlowFarneback(prev_gray, next_gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)

    # display optical flow with arrow
    if args.show_arrow:
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 8))
        ax1.set_title('prev_frame and prev2nextflow vector')
        ax1.imshow(prev_rgb)
        x, y, u, v = flow_vector(flow=flow, spacing=10, margin=0, minlength=1)
        ax1.quiver(x, y, u, v, angles='xy', scale_units='xy', scale=1, color=[0.0, 1.0, 0.0])

        ax2.set_title('next_frame')
        ax2.imshow(next_rgb)

        plt.show()

    if args.show:
        # 角度範囲のパラメータ
        ang_min = 0
        ang_max = 360
        _ang_min, _ang_max = adjust_ang(ang_min, ang_max)  # 角度の表現を統一する

        # HSV色空間の配列に入れる
        hsv = np.zeros_like(prev_frame, dtype='uint8')
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1], angleInDegrees=True)
        any_mag, any_ang = any_angle_only(mag, ang, ang_min, ang_max)
        hsv[..., 0] = 180*(any_ang - _ang_min) / (_ang_max - _ang_min)
        hsv[..., 1] = 255
        hsv[..., 2] = cv2.normalize(any_mag, None, 0, 255, cv2.NORM_MINMAX)
        flow_rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

        # 画像の原点にHSV色空間を埋め込み
        flow_rgb_display = np.copy(flow_rgb)
        hsv_cmap_rgb, *_ = hsv_cmap(_ang_min, _ang_max, 51)
        flow_rgb_display[0:hsv_cmap_rgb.shape[0], 0:hsv_cmap_rgb.shape[1]] = hsv_cmap_rgb

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 16))
        ax1.set_title('prev2nextflow rgb')
        ax1.imshow(flow_rgb_display)

        ax2.imshow(flow_rgb_display)
        x, y, u, v = flow_vector(flow=flow, spacing=10, margin=0, minlength=1)
        ax2.set_title('prev2nextflow rgb and vector')
        ax2.quiver(x, y, u, v, angles='xy', scale_units='xy', scale=1, color='w')

        plt.show()

    #if args.out is not None:
    #    cv2.imwrite(args.out, outimg)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(prog='main')
    parser.add_argument('src', type=str)
    parser.add_argument('ref', type=str)
    parser.add_argument('-o', '--out', default=None, type=str)
    parser.add_argument('-s', '--show', action='store_true')
    parser.add_argument('-S', '--show_arrow', action='store_true')
    parser.add_argument('--dispimg', action='store_true')
    args = parser.parse_args()
    opticalflow(args)
