# -*- coding: utf-8; mode: makefile -*-

# parameters
ROOT      = .
PIPLIST   = $(ROOT)/requirements.txt
ENV       = $(ROOT)/.venv

# command
PYTHON    = python3
VENV      = $(PYTHON) -m venv
ACTIVATE  = . $(ENV)/bin/activate
PIP       = $(ACTIVATE) && $(PYTHON) -m pip
LINT      = $(ACTIVATE) && $(PYTHON) -m flake8
TEST      = $(ACTIVATE) && $(PYTHON) -m unittest

MAIN      = ./cvlib/main.py

LENNA     = lenna.png
LENNA_URL = https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png

IMAGES    = $(LENNA) $(LENNA)

.PHONY: all test run env distclean clean

all: run

test:
	$(TEST) discover tests

run: $(IMAGES)
	$(ACTIVATE) && $(PYTHON) $(MAIN) $(IMAGES)

image: $(IMAGES)
$(IMAGES):
$(LENNA): 
	curl -o $@ $(LENNA_URL)

.PHONY: env

env: $(PIPLIST)
	$(VENV) $(ENV)
	$(PIP) install -U pip
	$(PIP) install -U wheel
	$(PIP) install -r $(PIPLIST)

.PHONY: clean distclean

clean:
	$(RM) -r ./rl/agent/__pycache__
	$(RM) -r ./tests/__pycache__

distclean: clean
	$(RM) -r $(ENV)
